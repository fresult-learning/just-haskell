module Haskell10 where

-- Type Classes
data Point = Point
  {coordinate :: (Double, Double)}

instance Num Point where
  (+) :: Point -> Point -> Point
  (+) (Point (x1, y1)) (Point (x2, y2)) = Point (x1 + x2, y1 + y2)

------ point1 + point2 --? Point { coordinate = (8.5, 9.0) }

instance Show Point where
  show :: Point -> String
  show (Point (x, y)) = "Point { x: " ++ show x ++ ", y: " ++ show y ++ " }"

------ point1 + point2 --? Point { x: 8.5, y: 9.0 }

-- Custom Type Classes
class Distance a where
  distance :: a -> Double

instance Distance Point where
  distance :: Point -> Double
  distance (Point (x, y)) = sqrt ((^) x 2 + (^) y 2)

------ distance point1 --? 7.433034373659253
------ distance point2 --? 5.0

point1 :: Point
point1 = Point (5.5, 5)

------ point1 --? Point { coordinate = (5.5, 5.0) }
------ coordinate point1 --? (5.5, 5.0)

point1' :: Point
point1' = Point {coordinate = (5.5, 5.0)}

------ point1' --? Point { coordinate = (5.5, 5.0) }
------ coordinate point1' --? (5.5, 5.0)

instance Eq Point where
  (/=) :: Point -> Point -> Bool
  (/=) (Point (x1, y1)) (Point (x2, y2))
    | x1 == x2 && y1 == y2 = False
    | otherwise = True

point2 :: Point
point2 = Point {coordinate = (3, 4)}
