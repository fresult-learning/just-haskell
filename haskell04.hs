module Haskell04 where

-- List
a :: [Int]
a = [1, 2, 3, 4, 5]

a' :: [Int]
a' = [1 .. 5] -- Range

a'' :: [Int]
a'' = [1, 3 .. 100]

-- List Comprehension
b :: [Int]
b = [x | x <- [1 .. 5]]

b' :: [Int]
b' = [x ^ 2 | x <- [1 .. 5]]

b'' :: [Int]
b'' = [double x | x <- [1 .. 5]]

b''' :: [Int] -- Predicate
b''' = [double x | x <- [1 .. 5], mod x 2 /= 0]

b'''' :: [Int]
b'''' = [double x | x <- [1 .. 5], isEven x]

double :: Int -> Int
double x = x * 2

isEven :: Int -> Bool
isEven x = x `mod` 2 == 0

-- List Concatenate
c :: [Int]
c = (++) [1, 3, 5, 7, 9] [0, 2, 4, 6, 8]

-- Index accessor (!!)
d :: Int
d = (!!) c 7

d' :: Int
d' = c !! 7

-- String
e :: Char
e = '3'

f :: [Char]
f = "34"

f' :: String
f' = "34"

f'' :: String
f'' = ['a' .. 'f'] ++ "099119"

g :: Bool
g = ['a' .. 'f'] == "abcdef" -- Left and Right stuff is the same

-- Cons (:)
h :: [Int]
h = [1, 2, 3, 4, 5]

h' :: [Int]
h' = 1 : 2 : 3 : 4 : 5 : []

h'' :: [Int]
h'' = 1 : (2 : (3 : (4 : (5 : []))))

h''' :: [Int]
h''' = (:) 1 ((:) 2 ((:) 3 ((:) 4 ((:) 5 []))))

h'''' :: [Char]
h'''' = (:) 'a' ['b' .. 'f']

i :: Bool
i = h'' == h'''
