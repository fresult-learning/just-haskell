module Haskell06 where

import Distribution.Simple.Utils (xargs)

-- Higher Order Functions
superFunction :: (Int -> Int -> Int) -> Int -> Int -> Int
superFunction f y x = f (x + 2) (y + 2)

squareAllElements :: [Int] -> [Int]
squareAllElements [] = []
squareAllElements (x : xs) = square x : squareAllElements xs

minusOneAllElements :: [Int] -> [Int]
minusOneAllElements [] = []
minusOneAllElements (x : xs) = (-) x 1 : minusOneAllElements xs

-- Generic Function
applyToAllElements :: (Int -> Int) -> [Int] -> [Int]
applyToAllElements f [] = []
applyToAllElements f (x : xs) = f x : applyToAllElements f xs

-- Generic Function with Polymorphism
applyToAllElements' :: (a -> b) -> [a] -> [b]
applyToAllElements' mapper [] = []
applyToAllElements' mapper (x : xs) = mapper x : applyToAllElements' mapper xs

-- More HoF
filterEven :: [Int] -> [Int]
filterEven [] = []
filterEven (x : xs)
  | mod x 2 == 0 = x : filterEven xs
  | otherwise = filterEven xs

filterIfMoreThan10 :: [Int] -> [Int]
filterIfMoreThan10 [] = []
filterIfMoreThan10 (x : xs) =
  if x > 10
    then x : filterIfMoreThan10 xs
    else filterIfMoreThan10 xs

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter predicate [] = []
myFilter predicate (x : xs)
  | predicate x = x : myFilter predicate xs
  | otherwise = myFilter predicate xs

-- Utility Function --
square :: Int -> Int
square x = x ^ 2
