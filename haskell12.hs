{-# LANGUAGE InstanceSigs #-}

module Haskell12 where

-- Applicative
data MyMaybe a = No | Yes a
  deriving (Show)

instance Functor MyMaybe where
  fmap :: (a -> b) -> MyMaybe a -> MyMaybe b
  fmap f No = No
  fmap f (Yes x) = Yes (f x)

instance Applicative MyMaybe where
  pure :: a -> MyMaybe a
  pure x = Yes x
  (<*>) :: MyMaybe (a -> b) -> MyMaybe a -> MyMaybe b
  (<*>) No _ = No
  (<*>) _ No = No
  (<*>) (Yes f) ma = f <$> ma
-- (<*>) (Yes f) ma = fmap f ma

------ pure (+ 2) <*> Yes 3 --? Yes 5

------ (+ 2) <$> Yes 3      --? Yes 5
