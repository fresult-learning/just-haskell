data Shape = Circle Radius | Square Side deriving Show

type Radius = Float
type Side = Float
type Area = Float

area :: Shape -> Area
area (Circle r) = pi * r * r
area (Square s) = s * s

data Person = Person String String Int deriving Show
-- Person "Korn" "Zilla" 18
---- Person "Korn" "Zilla" 18
-- :type Person
---- Person :: String -> String -> Int -> Person
data Person' = Person' FirstName LastName Age deriving Show
-- Person' "Korn" "Zilla" 18
---- Person' "Korn" "Zilla" 18
-- :type Person'
---- Person' :: FirstName -> LastName -> Age -> Person'


type FirstName = String
type LastName = String
type Age = Int

data Person'' = Person''
  { firstName :: String
  , lastName :: String
  , age :: Int
  } deriving Show
-- Person'' "Korn" "Zilla" 18
---- Person'' {firstName = "Korn", lastName = "Zilla", age = 18}
-- :type Person''
---- Person'' :: String -> String -> Int -> Person''
-- Person'' { lastName = "Sett", age = 18, firstName = "Sila" }
---- Person'' {firstName = "Sila", lastName = "Sett", age = 18}

-- korn = Person'' { lastName = "Sett", age = 18, firstName = "Sila" }
-- firstName korn
---- "Sila"
-- lastName korn
---- "Sett"
-- age korn
---- 18
