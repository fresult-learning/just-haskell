data Cloth = Cloth String deriving Show

data WornCloth = Worn Cloth
data WashedCloth = Washed Cloth
data DriedCloth = Dried Cloth
data IronedCloth = Ironed Cloth
data StoredCloth = Stored Cloth

ซัก :: WornCloth -> WashedCloth
ตาก :: WashedCloth -> DriedCloth
รีด :: DriedCloth -> IronedCloth
เก็บ :: IronedCloth -> StoredCloth

ซักตากรีดแล้วเก็บ :: WornCLoth -> StoredCloth
ซักตากรีดแล้วเก็บ = เก็บ . รีด . ตาก . ซัก

ซักแล้วเก็บผ้าทั้งกอง :: [WornCloth] -> [StoredCloth]
ซักแล้วเก็บผ้าทั้งกอง = map ซักตากรีดแล้วเก็บ

เสื้อผ้า :: [Cloth]
เสื้อผ้า = [Cloth "Shirt", Cloth "Jacket", Cloth "Blue Jeans"]

กองผ้าใส่แล้ว :: [WornCloth]
กองผ้าใส่แล้ว = map Worn เสื้อผ้า

งานพ่อบ้าน = ซักแล้วเก็บผ้าทั้งกอง กองผ้าใส่แล้ว
