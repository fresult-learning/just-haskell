data APIEndpoint = APIEndpoint BaseURL Path
                   deriving Show

newtype BaseURL = BaseURL String deriving Show
newtype Path = Path String deriving Show

apiEndpoint = APIEndpoint (BaseURL "https://example.com") (Path "/api/products/1")

baseURL :: APIEndpoint -> String
baseURL (APIEndpoint (BaseURL url) _) = url

apiPath :: APIEndpoint -> String
apiPath (APIEndpoint _ (Path path)) = path

-- baseURL (APIEndpoint (BaseURL "https://example.com") (Path "/api/products"))
---- "https://example.com"

-- apiPath (APIEndpoint (BaseURL "https://example.com") (Path "/api/products"))
---- "/api/products"
