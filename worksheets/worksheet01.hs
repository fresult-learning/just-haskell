module Worksheet1 where
import Data.Char (toLower, isAlpha, isSpace)
import Data.List (sort)

fibo :: [Integer]
fibo = 0 : 1 : [x + y | (x, y) <- zip fibo (tail fibo)]
------ fibo --? (It's a Lazy Evaluation) [0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,1597,2584,4181,6765,10946,17711,28657,46368,75025,121393,196418,317811, ...]

str1 = "Tom Marvolo Riddle"
str2 = "I am Lord Voldemort"

noSpace:: String -> String
noSpace = filter (not . isSpace)

lowerCase xs = [toLower x | x <- xs]

candidate::String -> String
candidate = Data.List.sort . lowerCase . noSpace

annagram:: String -> String -> Bool
annagram s1 s2 = candidate s1 == candidate s2

test = annagram str1 str2 -- True

elemCount::String -> Int
elemCount = length

isEven::Int -> Bool
isEven x = mod x 2 == 0

add2Tuple::(Int, Int) -> (Int, Int) -> (Int, Int)
add2Tuple (x1, y1) (x2, y2) = (x1 + x2, y1 + y2)

add2Tuple'::(Int, Int) -> (Int, Int) -> (Int, Int)
add2Tuple' t1 t2 = (fst t1 + fst t2, snd t1 + snd t2)

countElements::[a] -> String
countElements [] = "Empty"
countElements (_:[]) = "One"
countElements (_:_:[]) = "Two"
countElements _ = "Many"

tellHead::[Int] -> String
tellHead [] = "Empty"
tellHead (x:[]) = show x
tellHead (x:xs) = show x
    ++ " and "
    ++ show (length xs)
    ++ " number(s) more"

grade::Int -> String
grade x
    | x >= 90 = "A"
    | x >= 80 = "B"
    | x >= 70 = "C"
    | x >= 60 = "D"
    | otherwise = "F"

initials :: (String, String) -> String
initials name = [first] ++ "." ++ [last] :: String where
    first = head (fst name) :: Char
    last = head (snd name) :: Char


a :: [Int]
b :: [Int]
c :: Int -> [(Int, Int)]
a = [ x | x <- [1..] ]
b = [ x | x <- [1..9] ]
c takingA = [ (x, y) | x <- take takingA a, y <- b, x < y ]

isPrime :: Int -> Bool
isPrime x = [1, x] == [y | y <- [1..x], (mod x y) == 0]

fibs :: [Int]
fibs = 0 : 1 : [ a + b | (a, b) <- zip fibs (tail fibs) ]

fibs' :: Int -> [Int]
fibs' x = take x (0 : 1 : [ a + b | (a, b) <- zip (fibs' x) (tail (fibs' x)) ])

fibs'' :: Int -> [Int]
fibs'' x = take x fibs