module Worksheet11 where
import Control.Monad (when)
import System.Random (randomRIO)

data GameState = SnakeLadder
                  { board :: Board
                  , players :: [PlayerPosition]
                  , status :: Status
                  }

data Board = Board
              { goal :: Position
              , portals :: [Portal]
              }

type Portal = (Position, Position)
type Position = Int

type PlayerPosition = (Player, Position)
newtype Player = Player Int -- Integer ID

data Status = WonBy Player | Playing
newtype Dice = Dice Int
data PlayerMove = PlayerMove
                    { player :: Player
                    , move :: Int
                    }

rollDice :: Player -> Dice -> IO PlayerMove
rollDice _ = Dice <$> randomRIO (1, 6)

movePlayer :: PlayerMove -> GameState -> IO GameState
movePlayer playerMove gameState@(SnakeLadder board players status) =
  case status of
    WonBy _ -> return gameState
    Playing -> do
      let playerID = player playerMove
      let diceRoll = move playerMove
      let currentPosition = getPositionForPlayer playerID players
      let newPosition = currentPosition + diceRoll
      let updatedPlayers = updatePlayerPosition playerID newPosition players
      let updatedStatus = if newPosition >= goal (board gameState)
                            then WonBy playerID
                            else Playing
      return $ gameState { players = updatedPlayers, status = updatedStatus }
  where
    getPositionForPlayer :: Player -> [PlayerPosition] -> Position
    getPositionForPlayer _ [] = 1
    getPositionForPlayer targetPlayer ((p, pos):rest)
      | targetPlayer == p = pos
      | otherwise = getPositionForPlayer targetPlayer rest

    updatePlayerPosition :: Player -> Position -> [PlayerPosition] -> [PlayerPosition]
    updatePlayerPosition _ _ [] = []
    updatePlayerPosition targetPlayer newPosition ((p, pos):rest)
      | targetPlayer == p = (p, newPosition) : rest
      | otherwise = (p, pos) : updatePlayerPosition targetPlayer newPosition rest


gameLoop :: [Player] -> GameState -> IO ()
gameLoop players gameState@(SnakeLadder _ _ status) =
  case status of
    WonBy player -> putStrLn $ "Player " ++ show player ++ " has won!"
    Playing -> do
      player <- head players
      putStrLn $ "Player " ++ show player ++ "'s turn!"
      dice <- rollDice player
      let playerMove = PlayerMove player (getDiceValue dice)
      newGameState <- movePlayer playerMove gameState
      gameLoop (tail players ++ [player]) newGameState
  where
    getDiceValue :: Dice -> Int
    getDiceValue (Dice value) = value
