filter' :: (a -> Bool) -> [a] -> [a]
filter' _ []   = []
filter' pred (x : xs)
  | pred x    = x : filtered
  | otherwise = filtered
  where
    filtered = filter' pred xs

isA :: Char -> Bool
isA x = x == 'A' || x == 'a'

map' :: (a -> b) -> [a] -> [b]
map' _ []       = []
map' f (x : xs) = f x : map f xs

foldr' :: (a -> b -> b) -> b -> [a] -> b
foldr' _ b []       = b
foldr' f b (x : xs) = f x (foldr' f b xs)
-- foldr' (+) 0 [1,2,3,4]
-- 1 + ((foldr' (+) 0 [2,3,4]))
-- 1 + 2 + (foldr' (+) 0 [3,4])
-- 1 + 2 + 3 + (foldr' (+) 0 [4])
-- 1 + 2 + 3 + 4 + (foldr' (+) 0 [])
-- 1 + 2 + 3 + 4 + 0

concat' :: [[a]] -> [a]
concat' [] = []
concat' (xs:xss) = xs ++ concat' xss
-- ghci> concat' [[1,2], [], [1, 2, 3], [], [1, 2], [1, 2], [], [1]]
---- [1,2,1,2,3,1,2,1,2,1]

(+++) :: [a] -> [a] -> [a]
[] +++ ys       = ys
(x : xs) +++ ys = x : (xs +++ ys)
-- ghci> [1..3] +++ []
---- [1,2,3]
-- ghci> [1..3] +++ [1..3]
---- [1,2,3,1,2,3]

reverse' :: [a] -> [a]
reverse' []       = []
reverse' (x : xs) = reverse' xs ++ [x]

elem' :: Eq a => a -> [a] -> Bool
elem' _ []      = False
elem' target (x : xs)
  | target == x = True
  | otherwise   = elem' target xs

minimum' :: Ord a => [a] -> a
minimum' []       = error "Empty list has no minimum element"
minimum' [x]      = x
minimum' (x : xs) = if x < minXs then x else minXs
  where minXs = minimum' xs
-- ghci> minimum' [1, 2, -1, 4, 3, 5]
---- -1

uniq :: Eq a => [a] -> [a]
uniq [] = []
uniq (x : xs) = x : uniq(xs `without` x)
  where list `without` e = filter (/= e) list
-- ghci> uniq [1,2,1,1,2,3]
---- [1,2,3]

-- map (* 2) [1, 2, 3] = [4, 5, 6]
-- (* 2) 1 : map (* 2) [2, 3]
-- (* 2) 1 : (* 2) 2 : map (* 2) [3]
-- (* 2) 1 : (* 2) 2 : (* 2) 3 : []
-- 2 : 4 : 6 : []
-- [2, 4, 6]

sum' :: (Foldable t, Num a) => t a -> a
sum' = foldr (+) 0
-- ghci> map' sum' [[1..3], [4..7], [8..10]]
---- [6,22,27]

-- map sum [[1..3], [4..7], [8..10]]
-- sum [1..3] : map sum [[4..7], [8..10]]
-- sum [1..3] : sum [4..7] : map sum [[8..10]]
-- sum [1..3] : sum [4..7] : sum [8..10] : map sum []
-- sum [1..3] : sum [4..7] : sum [8..10] : []
-- (1 + 2 + 3) : (4 + 5 + 6 + 7) : (8 + 9 + 10) + 0
-- [6, 22, 7]
