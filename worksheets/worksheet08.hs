data Score = Score { midterm :: Int
                   , final :: Int
                   , homeworks :: [Int]
                   , projects :: [Int]
                   }
                   deriving Show

scores1 = Score 54 77 [8, 8, 3, 5] [12, 8]
scores2 = Score 81 72 [10, 8, 9, 9] [14, 9]

-- midterm scores1
---- 54
-- final scores1
---- 77
-- homeworks scores1
---- [8, 8, 3, 5]
-- projects scores1
---- [12, 8]

-- midterm :: Score -> Int
-- midterm (Score m _ _ _) = m

-- final :: Score -> Int
-- final (Score _ f _ _) = f

-- homeworks :: Score -> Int
-- homeworks (Score _ _ hs _) = hs

-- projects :: Score -> Int
-- projects (Score _ _ _ ps) = ps
