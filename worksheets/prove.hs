

{-
(map g . map f) (x : xs)  = map f (map g (x : xs))         -- Function composition (f . g) x = f(g x)
                          = map f (g x : map g xs)         -- Definition of map
                          = f (g x) : map f (map g xs)
                          = (f . g) x : (map f . map g) xs -- Function composition f(g x) = (f . g) x & Induction
                          = map (f . g) (x : xs)
(map g . map f) (x : xs)  = map (f . g) (x : xs)
-}








map :: (a -> b) -> [a] -> [b]
map _ []       = []
map f (x : xs) = f x : map f xs

-- map (* 2) [1..3]

(map f . map g) (x : xs)  = map f (map g (x : xs))
                          = map f (g x : (map g xs))
                          = f (g x) : (map f (map g xs))
                          = (f . g) x : (map f . map g) xs
                          = map (f . g) (x : xs)



map :: (a -> b) -> [a] -> [b]
map _ []       = []
map f (x : xs) = f x : map f xs


(map g . map f) (x : xs)  = map g (map f (x : xs))
                          = map g (f x : (map f xs))
                          = g(f x) : map g (map f xs)
                          = (g . f) x : (map g . map f) xs
                          = map (g . f) (x : xs)


map f . filter p . concat  = map . (concat . map (filter p))
                          = map . concat . map (filter p)
                          = (concat . map (map f)) . map (filter p)
                          = concat . map (map f) . map (filter p)
                          = concat . (map (map f . filter p))
                          = concat . map (map f . filter p)


map f (xs ++ ys)  = map f xs ++ map f ys

Induction on xs - case []
map f ([] ++ ys)  = [] ++ map f ys
                  = map f ys

Induction on xs - case (x : xs)
map f ((x : xs) ++ ys)  = f x : map f (xs ++ ys)
                        = f x : map f xs ++ map f ys
                        = f x : map f (xs ++ ys)