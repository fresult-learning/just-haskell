divide :: Float -> Float -> Either String Float
divide _ 0 = Left "undefined: divided by zero"
divide m n = Right $ m / n

-- type Result a = Result a

-- data Response a = ErrorMessage String | Result a
data Response' e a = Error e | Result a

-- data (,) a b = (,) a b
-- data Product a b = Pair a b
  -- deriving Show

type Name = String
type JerseyNo = Int

data Player = Player Name
data Position = GK | DF | MF | FW
data Roster = Roster [(JerseyNo, Player, Position)]
data Team = Team { name :: Name, roster :: Roster }
data Match = Match { homeTeam :: Team, awayTeam :: Team }
