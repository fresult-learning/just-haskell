data Day = Mon | Tue | Wed | Thu | Fri | Sat | Sun -- deriving Show
  deriving (Show, Eq, Ord)

-- instance Show Day where
--   show Mon = "Mon"
--   show Tue = "Tue"
--   show Wed = "Wed"
--   show Thu = "Thu"
--   show Fri = "Fri"
--   show Sat = "Sat"
--   show Sun = "Sun"

-- instance Eq Day where
--   Mon == Mon = True
--   Tue == Tue = True
--   Wed == Wed = True
--   Thu == Thu = True
--   Fri == Fri = True
--   Sat == Sat = True
--   Sun == Sun = True
--   _   == _   = False

-- instance Ord Day where
--   Mon <= _   = True
--   Tue <= Mon = False
--   Tue <= _   = True
--   Wed <= Mon = False
--   Wed <= Tue = False
--   Wed <= _   = True
--   Thu <= Mon = False
--   Thu <= Tue = False
--   Thu <= Wed = False
--   Thu <= _   = True
--   ....
