data Tree a = EmptyTree | Node a (Tree a) (Tree a)
  deriving Show

singletonNode :: a -> Tree a
singletonNode x = Node x EmptyTree EmptyTree

treeInsert :: (Ord a) => a -> Tree a -> Tree a
treeInsert x EmptyTree = singletonNode x
treeInsert x (Node a left right)
  | x == a = Node x left right
  | x < a = Node a (treeInsert x left) right
  | x > a = Node a left (treeInsert x right)

nums :: [Int]
nums = [8,6,4,1,5,5,6,7,3,5]
tree :: Tree Int
tree = foldr treeInsert EmptyTree nums
-- tree
---- Node 5 (Node 3 (Node 1 EmptyTree EmptyTree) (Node 4 EmptyTree EmptyTree)) (Node 7 (Node 6 EmptyTree EmptyTree) (Node 8 EmptyTree EmptyTree))

flatten :: Tree a -> [a]
flatten EmptyTree = []
flatten (Node a left right) = [a] ++ flatten left ++ flatten right
-- flatten tree
---- [5,3,1,4,7,6,8]

count :: Tree a -> Int
count = length . flatten
-- count tree
---- 7