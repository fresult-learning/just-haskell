-- {-# LANGUAGE GADTs, StandaloneDeriving #-}
{-# LANGUAGE  ExplicitForAll
            , ExistentialQuantification
            , StandaloneDeriving
#-}

data Circle = Circle { radius :: Float }
              deriving (Show, Eq)
data Square = Square { side :: Float }
              deriving (Show, Eq)
data Rectangle = Rectangle { height :: Float, width :: Float }
                 deriving (Show, Eq)
data Ellipse = Ellipse { a :: Float, b :: Float }
             deriving (Show, Eq)

data Shape = forall a. (Show a, Eq a, HasArea a) => Shape a
deriving instance Show Shape

class HasArea a where
  area :: a -> Float

instance HasArea Circle where
  area (Circle r) = pi * r * r

instance HasArea Square where
  area (Square s) = s * s

instance HasArea Rectangle where
  area (Rectangle h w) = h * w

instance HasArea Ellipse where
  area (Ellipse a b) = pi * a * b

instance HasArea Shape where
  area (Shape a) = area a

instance Eq Shape where
  a == b = (show a) == (show b)

instance Ord Shape where
  Shape a <= Shape b = (area a) <= (area b)

c5 = Circle 5
c10 = Circle 10
s5 = Square 5
s10 = Square 10
rect = Rectangle 7.5 10
ellipse = Ellipse 8.4 12.1

-- test1 = sort [area c5, area c10, area s5, area s10, area rect]
-- test2 = sort [c5, c10, s5, s10, rect]
