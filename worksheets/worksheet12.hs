data Nat = Zero | Succ Nat deriving (Show, Ord, Eq)

instance Num Nat where
  m + Zero        = m
  m + (Succ n)    = Succ (m + n)

  m * Zero        = Zero
  m * (Succ n)    = m * n + m

  abs n           = n

  signum Zero     = Zero
  signum (Succ n) = Succ Zero

  m - Zero            = m
  Zero - (Succ n)     = Zero

  -- m - (Succ n) = m - n
  (Succ m) - (Succ n)
    | m <= n = Zero
    | m >  n = m - n

  fromInteger x
    | x <= 0      = Zero
    | otherwise   = Succ (fromInteger (x - 1))

toInteger' :: Nat -> Integer
toInteger' Zero     = 0
toInteger' (Succ n) = 1 + toInteger' n
-- ghci> toInteger' ( Succ (Succ (Succ (Succ Zero))) * (Succ (Succ Zero)) )

toNat :: Int -> Nat
toNat 0 = Zero
toNat x = Succ $ toNat (x - 1)

toNats :: [Int] -> [Nat]
toNats = map toNat
-- ghci> toNats [1..3]
---- [Succ Zero,Succ (Succ Zero),Succ (Succ (Succ Zero))]
-- ghci> sum' (toNats [1..3])
---- Succ (Succ (Succ (Succ (Succ (Succ Zero)))))

count :: Num a => [a] -> Int
count []       = 0
count (_ : xs) = 1 + count xs

count' :: [a] -> Nat
count' []       = Zero
count' (_ : xs) = Succ (count' xs)
-- ghci> count' [1..4]
---- Succ (Succ (Succ (Succ Zero)))
-- ghci> toInteger' it
---- 4

count'' :: Num b => [a] -> b
count'' []       = 0
count'' (_ : xs) = 1 + count'' xs
-- ghci> count'' [1..5] :: Nat
---- Succ (Succ (Succ (Succ (Succ Zero))))

sum' :: Num a => [a] -> a
sum' [] = 0
sum' (x : xs) = x + sum' xs
-- ghci> sum' (toNats [1..3])
---- Succ (Succ (Succ (Succ (Succ (Succ Zero)))))

class Printable a where
  printt :: a -> String

instance Printable String where
  printt str = "The string is: " ++ str

instance Printable Nat where
  printt nat = "The nat is: " ++ show nat
