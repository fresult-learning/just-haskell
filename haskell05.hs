module Haskell05 where

-- Pattern Matchings
isNine :: Int -> Bool
isNine 9 = True
isNine _ = False -- Wildcard

isSevenOrNine :: Int -> Bool
isSevenOrNine 7 = True
isSevenOrNine 9 = True
isSevenOrNine _ = False

-- x :: Int
-- xs :: [Int]
-- (x : xs) = [1, 2, 3, 4, 5]

-- Recursion by Pattern Matching
sumList :: [Int] -> Int
sumList [] = 0
sumList (x : xs) = x + sumList xs

sumList' :: [Int] -> Int
sumList' [] = 0
sumList' nums = head nums + sumList' (tail nums)

multiplyList :: [Int] -> Int
multiplyList [] = 1
multiplyList (x : xs) = x * multiplyList xs

multiplyList' :: [Int] -> Int
multiplyList' [] = 1
multiplyList' nums = head nums * multiplyList' (tail nums)

-- Recursion by Condition
---- IF - THEN - ELSE
sumListWithCondition :: [Int] -> Int
sumListWithCondition [] = 0
sumListWithCondition (x : xs) =
  if isModuloOf 3 5 x
    then x + sumListWithCondition xs
    else sumListWithCondition xs

---- GUARDS
sumListWithCondition' :: [Int] -> Int
sumListWithCondition' [] = 0
sumListWithCondition' (x : xs)
  | isModuloOf 3 5 x = x + sumListWithCondition' xs
  | otherwise = sumListWithCondition' xs

isModuloOf :: Int -> Int -> Int -> Bool
isModuloOf y x e = mod e x == 0 && mod e y == 0

-- EXERCISE
---- 1. Reverse
myReverse :: [x] -> [x]
myReverse [] = []
myReverse (x : xs) = myReverse xs ++ [x]
------ Breakdown myReverse
-- myReverse [1, 2, 3]
-- myReverse [2, 3]           ++ [1]
-- myReverse [3]       ++ [2] ++ [1]
-- myReverse [] ++ [3] ++ [2] ++ [1]
---- [] ++ [3] ++ [2] ++ [1]

---- 2. Has Item in List
hasItemInList :: Int -> [Int] -> Bool
hasItemInList y [] = False
hasItemInList y (x : xs)
  | y == x = True
  | otherwise = hasItemInList y xs
------ Breakdown hasItemInList
-- hasItemInList 3 [1, 2, 3]  -> 1 != 3 -> False
-- hasItemInList 3 [2, 3]     -> 2 != 3 -> False
-- hasItemInList 3 [3]        -> 3 == 3 -> True
