module Haskell07 where

-- Anonymous Functions (Lambda (λ))
---- example with Function
double :: Int -> Int
double x = x * 2

-- λ
---- (\x -> «function_body»)
lambdaDouble :: Int -> Int
lambdaDouble = (\x -> x * 2)

-- Folding
---- foldr
---- foldl

---- Problems
mySum :: [Int] -> Int
mySum [] = 0
mySum (x : xs) = (+) x (mySum xs)

myMultiply :: [Int] -> Int
myMultiply [] = 1
myMultiply (x : xs) = (*) x (myMultiply xs)

myLength :: [a] -> Int
myLength [] = 0
myLength (_ : xs) = 1 + myLength xs

------ More Abstraction for 3 above functions
foldRight :: (a -> b -> b) -> b -> [a] -> b
foldRight folding acc [] = acc
foldRight folding acc (x : xs) = folding x (foldRight folding acc xs)

-------- USAGE --------
-- foldRight (+) 0 [1..5] -- same as mySum [1..5]
-- foldRight (*) 1 [1..5] -- same as myMultiply [1..5]
-- foldRight (*) 1 [1..5] -- same as myMultiply [1..5]

-- foldRight (\x acc -> acc + 1) 0 (['A'..'Z'] ++ ['a'..'z']) -- 52
-- doCount _ acc = acc + 1
-- foldRight doCount 0 (['A'..'Z'] ++ ['a'..'z'])             -- 52


-------- BREAKDOWN foldRight (foldr)
-- foldRight (+) 0 [1, 2, 3]
-- (+) 1 (foldRight (+) 0 [2, 3])
-- (+) 1 ((+) 2 (foldRight (+) 0 [3]))
-- (+) 1 ((+) 2 ((+) 3 (foldRight (+) 0 [])))
-- (+) 1 ((+) 2 ((+) 3 (0)))
-- (+) 1 ((+) 2 (3))
-- (+) 1 (5)
-- 6
---- ∴ 1 + 2 + 3 = 6

-- foldr (-) 0 [1, 2, 3]
---- 1 - (2 - (3 - 0))
-- foldl (-) 0 [1, 2, 3]
---- ((0 - 1) - 2) - 3



-- foldRight (\x acc -> acc + 1) 0 [1, 2, 3]     -> acc = 0 + 1 (= 1)
-- foldRight (\x acc -> acc + 1) 1 [2, 3]        -> acc = 1 + 1 (= 2)
-- foldRight (\x acc -> acc + 1) 2 [3]           -> acc = 2 + 1 (= 3)
---- ∴ 1 + 1 + 1 = 3


-- EXERCISE
myMap :: (a -> b) -> [a] -> [b]
myMap mapper [] = []
myMap mapper (x : xs) = mapper x : myMap mapper xs

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter predicate [] = []
myFilter predicate (x : xs)
  | predicate x = x : myFilter predicate xs
  | otherwise = myFilter predicate xs

---- map by foldr
mapByFoldr :: Foldable t1 => (t2 -> a) -> t1 t2 -> [a]
mapByFoldr mapper xs = foldr (\x acc -> mapper x : acc) [] xs

---- filter by foldr
filterByFoldr :: Foldable t => (a -> Bool) -> t a -> [a]
filterByFoldr predicateFn xs = foldr (\x acc ->
  if predicateFn x
    then x : acc
    else acc) [] xs
