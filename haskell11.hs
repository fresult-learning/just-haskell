module Haskell10 where

import Data.Char (toUpper)

data MyMaybe a = No | Yes a
  deriving (Show)

instance Functor MyMaybe where
  fmap :: (a -> b) -> MyMaybe a -> MyMaybe b
  fmap f No = No
  fmap f (Yes x) = Yes (f x)
------ fmap (*2) No    --? No
------ fmap (*2) Yes 10 --? Yes 20

plusMyMaybe :: Num a => a -> MyMaybe a -> MyMaybe a
plusMyMaybe x No = No
plusMyMaybe x (Yes y) = Yes ((+) x y)
------ plusMyMaybe 3 No      --? No
------ plusMyMaybe 3 (Yes 4) --? Yes 7

multiplyMyMaybe :: Num a => a -> MyMaybe a -> MyMaybe a
multiplyMyMaybe x No = No
multiplyMyMaybe x (Yes y) = Yes ((*) x y)
------ multiplyMyMaybe 3 No      --? No
------ multiplyMyMaybe 3 (Yes 4) --? Yes 12

powTwoByMyMaybeX :: (Integral a, Num b) => a -> MyMaybe b
powTwoByMyMaybeX x = fmap (2 ^) (Yes x)
------ powTwoByMyMaybeX 3 --? Yes 8

text :: [Char]
text = "haskell"

upperText :: [Char]
upperText = fmap toUpper text -- "Haskell"
------ map toUpper "haskell"              --? "HASKELL"
------ fmap (map toUpper) (Yes "haskell") --? Yes "HASKELL"

upperTextMyMaybe :: MyMaybe [Char]
upperTextMyMaybe = fmap (fmap toUpper) (Yes "Hello")
------ upperTextMyMaybe --? Yes "HASKELL"

upperTextMyMaybe' :: MyMaybe [Char]
upperTextMyMaybe' = fmap toUpper <$> Yes "haskell"
------ upperTextMyMaybe' --? Yes "HASKELL"

data Chain a = GenesisBlock | Block (Chain a) a
  deriving Show

chain1 :: Chain Int
chain1 = Block (Block GenesisBlock 4) 8
------ GenesisBlock, 2, 4 --?

instance Functor Chain where
  fmap :: (a -> b) -> Chain a -> Chain b
  fmap f GenesisBlock = GenesisBlock
  fmap f (Block chain a) = Block (fmap f chain) (f a)
------ (2 ^) <$> chain1 --? Block (Block GenesisBlock 16) 256
