module Haskell08 where

-- data Bool = True | False
---- Left hand-side -> Type Constructor
---- Right hand-side -> Data Constructor
data Choice = Yes | No deriving (Show)

andChoice :: Choice -> Choice -> Choice
andChoice Yes Yes = Yes
andChoice Yes No = No
andChoice No Yes = No
andChoice No No = No

------ andChoice Yes No --? No
andChoice' :: Choice -> Choice -> Choice
andChoice' Yes Yes = Yes
andChoice' _ _ = No

-- Data Constructor \w arguments
data Person = Person FirstName LastName Age
  deriving (Show)

------------- Person String String Age deriving Show
john :: Person
john = Person "John" "Doh" 32

-- Type Synonyms
type FirstName = String
type LastName = String
type Age = Int

-- getPersonFirstName :: Person -> FirstName
-- getPersonFirstName (Person firstName _ _) = firstName
-- ------ getPersonFirstName john            --? John
-- getPersonLastName :: Person -> FirstName
-- getPersonLastName (Person _ lastName _)   = lastName
-- ------ getPersonLastName john             --? "Doh"
-- getPersonAge :: Person -> Age
-- getPersonAge (Person _ _ age)             = age
-- ------ getPersonAge john                  --? 32
-------- USAGE by Lambda
---- (\(Person firstName _ _) -> firstName) john --? "John"
---- (\(Person _ lastName _) -> lastName) john   --? "Doh"
---- (\(Person _ _ age) -> age) john             --? 32


-- Record Syntax
data Person' = Person'
  { firstName :: String,
    lastName :: String,
    age :: Int
  }
  deriving (Show)

john' :: Person'
john' =
  Person'
    { firstName = "John",
      lastName = "Doh",
      age = 32
    }
------ firstName john' --? "John"
------ lastName john' --? "Doh"
------ age john' --? 32

-- Polymorphic Data Types
---- Data Constructor (RHS) can have zero or more arguments.
---- Type Constructor (LHS), too.
data Square a = Square a deriving (Show)

intSquare :: Square Int
intSquare = Square 5

doubleSquare :: Square Double
doubleSquare = Square 4.5

intSquare' :: Int -> Square Int
intSquare' x = Square x

doubleSquare' :: Double -> Square Double
doubleSquare' x = Square x

numSquare :: a -> Square a
numSquare = Square

area :: Num a => Square a -> a
area (Square edge) = (^) edge 2

-------- USAGE --------
------ area intSquare           --? 25
------ area (intSquare' 5)      --? 25
------ area doubleSquare        --? 20.25
------ area (doubleSquare' 4.5) --? 20.25
------ area (numSquare 5)       --? 25
------ area (numSquare 4.5)     --? 20.25

-- Recursive (+ Polymorphic) Data Types
data Chain a = GenesisBlock | Block (Chain a) a deriving (Show)

intChain1 :: Chain Int
intChain1 = GenesisBlock

intChain2 :: Chain Int
intChain2 = Block GenesisBlock 3

intChain3 :: Chain Int
intChain3 = Block (Block GenesisBlock 3) 17

intChain4 :: Chain Int
intChain4 = Block (Block (Block intChain2 17) 14) 18

boolChain :: Chain Bool
boolChain = Block (Block (Block (Block (Block GenesisBlock True) False) True) False) False

---- Recursive data type Length implementation
chainLength :: Chain a -> Int
chainLength GenesisBlock = 0
chainLength (Block chain value) = 1 + chainLength chain

-------- USAGE --------
------ chainLength intChain1 --? 0
------ chainLength intChain2 --? 1
------ chainLength intChain3 --? 2
------ chainLength intChain4 --? 3
------ chainLength boolChain --? 5
