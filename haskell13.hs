{-# LANGUAGE InstanceSigs #-}

module Haskell13 where

-- Monad
---- Functor
-------- fmap <$>
---- Applicative
-------- pure
-------- <*>
---- Monad
data MyMaybe a = No | Yes a
  deriving (Show)

instance Functor MyMaybe where
  fmap :: (a -> b) -> MyMaybe a -> MyMaybe b
  fmap f No = No
  fmap f (Yes x) = Yes (f x)

instance Applicative MyMaybe where
  pure :: a -> MyMaybe a
  pure x = Yes x
  (<*>) :: MyMaybe (a -> b) -> MyMaybe a -> MyMaybe b
  (<*>) No _ = No
  (<*>) _ No = No
  (<*>) (Yes f) ma = f <$> ma

instance Monad MyMaybe where
  (>>=) :: MyMaybe a -> (a -> MyMaybe b) -> MyMaybe b
  (>>=) No _ = No
  (>>=) (Yes a) fAmb = fAmb a

safeHead :: [a] -> MyMaybe a
safeHead [] = No
safeHead xs = Yes ((!!) xs 0) -- Yes (head xs)

safeDiv :: Int -> Int -> MyMaybe Int
safeDiv _ 0 = No
safeDiv x y = Yes (div x y)

computeAndContinue :: [Int] -> Int -> MyMaybe Int
computeAndContinue xs y =
  case safeHead xs of
    No -> No
    Yes x -> case safeDiv x y of
      No -> No
      Yes n -> Yes n
------ computeAndContinue [10..20] 2 --? Yes 5
------ computeAndContinue [10..20] 0 --? No

computeAndContinue' :: [Int] -> Int -> MyMaybe Int
computeAndContinue' xs y =
  safeHead xs >>= \x
    -> safeDiv x y >>= \n
      -> Yes n
------ computeAndContinue' [10..20] 2 --? Yes 5
------ computeAndContinue' [10..20] 0 --? No

-- Do Notation
computeAndContinue'' :: [Int] -> Int -> MyMaybe Int
computeAndContinue'' xs y = do
  x <- safeHead xs
  n <- safeDiv x y
  Yes n
------ computeAndContinue'' [10..20] 2 --? Yes 5
------ computeAndContinue'' [10..20] 0 --? No

---- Conclusion
------ Functor
-------- fmap (+ 2) (Just 5) -- Just 7
-------- (+ 2) <$> Just 5    -- Just 7
------ Applicative
-------- (+) <$> (Just 2) <*> (Just 3)  -- Just 5
-------- fmap (+) (Just 2) <*> (Just 3) -- Just 5
------ Monad
-------- (+) <$> Yes 2 <*> Yes 3 >>= (\x -> Yes (x ^ 2)) -- Just 25
-------- (+) <$> Yes 2 <*> Yes 3 >>= (\x -> No)          -- No
-------- (+) <$> Yes 2 <*> No >>= (\x -> Yes (x ^ 2))    -- No
