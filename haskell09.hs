module Haskell09 where

import Prelude hiding (fst, snd)

-------- Built-in Data Types --------
-- Tuples (Pairs)
my1stTuple :: (Integer, Integer)
my1stTuple = (1, 2)

my1stTuple' :: (Integer, Integer)
my1stTuple' = (,) 1 2
------ So, `(1, 2) == (,) 1 2` --? True

fst :: (a, b) -> a
fst (a, _) = a
------ fst ((,) 1 True) --? 1

snd :: (a, b) -> b
snd (_, b) = b
------ snd ((,) 1 True) --? True

---- Example real world usage for Tuples
------ Game
type Point = (Int, Int) -- Point Data Type
moveUp :: Point -> Point
moveUp (x, y) = (x, y + 1)

moveRight :: Point -> Point
moveRight (x, y) = (x + 1, y)

-- Maybe
safeDiv :: Int -> Int -> Maybe Int
safeDiv _ 0 = Nothing
safeDiv x y = Just (div x y)
------ safeDiv 3 0 --? Nothing
------ safeDiv 3 2 --? Just 1

safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x : _) = Just x
------ safeHead []     --? Nothing
------ safeHead [3]    --? Just 3
------ safeHead [4, 5] --? Just 4

safeTail :: [a] -> Maybe [a]
safeTail [] = Nothing
safeTail (_ : xs) = Just xs
------ safeTail []      --? Nothing
------ safeTail [1]     --? Just []
------ safeTail [1..10] --? Just [2, 3, 4, 5, 6, 7, 8, 9, 10]
------ safeTail [10..9] --? Nothing

-- Either
safeDiv' :: Int -> Int -> Either String Int
safeDiv' _ 0 = Left "Cannot divide by zero."
safeDiv' x y = Right (div x y)
------ safeDiv' 4 0 --? Left "Cannot divide by zero."
------ safeDiv' 4 3 --? Right 1

safeHead' :: [a] -> Either String a
safeHead' [] = Left "List cannot be empty."
safeHead' (x : _) = Right x
------ safeHead' [] --? Left "List cannot be empty."
------ safeHead' [3..10] --? Right 3
------ safeHead' [5] --? Right 5

safeTail' :: [a] -> Either String [a]
safeTail' [] = Left "List cannot be empty."
safeTail' (_ : xs) = Right xs
------ safeTail' []      --? Left "List cannot be empty."
------ safeTail' [1]     --? Right []
------ safeTail' [1..10] --? Right [2, 3, 4, 5, 6, 7, 8, 9, 10]
------ safeTail' [10..9] --? Left "List cannot be empty."
