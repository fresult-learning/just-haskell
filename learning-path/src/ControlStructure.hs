myIf True thenFunc elseFunc = thenFunc
myIf False thenFunc elseFunc = elseFunc

main :: IO ()
main =
    let x = 5 :: Integer
    in print $ myIf (x == 5) "is Five" "is not Five"