module ColourCmykRgb (Colour) where

data Colour = CMYK Float Float Float Float | RGB Int Int Int deriving Show

colorRGB :: Colour
colorRGB = RGB 123 132 231

colorCMYK :: Colour
colorCMYK = CMYK 1.2 2.3 3.4 4.5

-- USAGEs
-- colorRGB  -- RGB 123 132 231
-- colorCMYK -- RGB 123 132 231
