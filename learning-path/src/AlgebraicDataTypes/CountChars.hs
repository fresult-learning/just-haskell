type Str = [Char]
type Count = Int

countChars :: Str -> Count
countChars = length

main :: IO ()
main = do
  print $ countChars "KornZilla" -- 9
