data Compass = North | East | West | South
  deriving (Show, Enum, Ord, Eq)
-- data Fruit = Apple | Papaya | Banana | Orange

-- instance Show Compass where
--   show North = "N"
--   show East = "E"
--   show South = "S"
--   show West = "W"

-- instance Eq Compass where
--   North == North = True
--   North == _ = False
--   East == East = True
--   East == _ = False
--   West == West = True
--   West == _ = False
--   South == South = True
--   South == _ = False
--   North /= North = False
--   North /= _ = True
--   East /= East = False
--   East /= _ = True
--   West /= West = False
--   West /= _ = True
--   South /= South = False
--   South /= _ = True

main :: IO ()
main = do
  print $ North > East -- False
  print $ South == South -- True
  print $ East /= North -- True
  print $ West /= West -- False
  print $ East == North -- False
