data Expression = Number Int
                | Add Expression Expression
                | Subtract Expression Expression
                deriving (Show, Ord, Eq)

main :: IO ()
main = do
  print $ Number 1 > Number 2 -- False
  print $ Number 2 > Number 0 -- True
  print $ Number 4 == Number 4 --True
  print $ Number 3 /= Number 5 -- True
  print $ Add (Number 1) $ Subtract (Number 10) $ Number 5 -- Add (Number 1) (Subtract (Number 10) (Number 5))
