module ColourRgbRecord (Colour) where

data Colour = RGB
    { red :: Int
    , green :: Int
    , blue :: Int
    } deriving Show

color :: Colour
color = RGB 234 123 189

-- red color -- 234
-- green color -- 123
-- blue color -- 189