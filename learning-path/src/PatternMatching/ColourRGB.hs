data Colour = RGB Int Int Int deriving Show

{-% Simple Pattern Maching %-}
red :: Colour -> Int
red \(RGB r _ _) -> r

green :: Colour -> Int
green (RGB _ g _) = g

blue :: Colour -> Int
blue \(RGB _ _ b) -> b

{-% Nested Pattern Matching %-}
data Pixel = Pixel Int Int Int Colour deriving Show

pixelRed :: Pixel -> ((Int, Int, Int), Int)
pixelRed (Pixel width len height (RGB r _ _)) = ((width, len, height), r)

pixelGreen :: Pixel -> ((Int, Int, Int), Int)
pixelGreen (Pixel width len height (RGB _ g _)) = ((width, len, height), g)

pixelBlue :: Pixel -> ((Int, Int, Int), Int)
pixelBlue (Pixel width len height (RGB _ _ b)) = ((width, len, height), b)

main :: IO ()
main = do
  let color = RGB 10 20 30
  let pixel = Pixel 100 100 100 $ RGB 85 170 255
  print color
  print $ red color
  print $ green color
  print $ blue color
  print $ pixelRed pixel
  print $ pixelGreen pixel
  print $ pixelBlue pixel