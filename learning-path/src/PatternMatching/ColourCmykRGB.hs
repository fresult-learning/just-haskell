data Colour = CMYK Float Float Float Float | RGB Int Int Int

colorModel :: Colour -> String
colorModel (RGB _ _ _) = "RGB"
colorModel (CMYK _ _ _ _) = "CMYK"

colorModel' :: Colour -> String
colorModel' cm =
  case cm of CMYK _ _ _ _ -> "CMYK'"
             RGB _ _ _    -> "RGB'"

colorModel'' :: Colour -> String
colorModel'' cm =
  case cm of CMYK _ _ _ _ -> "CMYK''"
            --  _            -> "Nothing''"

its :: String -> String
its str = "It's " ++ str

main :: IO ()
main = do
  let cmyk = colorModel $ CMYK 1.2 2.3 3.4 4.5
  let rgb = colorModel $ RGB 12 123 234
  let cmyk' = colorModel' $ CMYK 2.4 3.5 4.6 5.7
  let rgb' = colorModel' $ RGB 234 012 123
  let cmyk'' = colorModel'' $ CMYK 2.4 3.5 4.6 5.7
  let rgb'' = colorModel'' $ RGB 234 012 123
  putStrLn $ its cmyk
  putStrLn $ its rgb
  putStrLn $ its cmyk'
  putStrLn $ its rgb'
  putStrLn $ its cmyk''
  -- putStrLn $ its rgb''
