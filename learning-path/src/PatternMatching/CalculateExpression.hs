data Expression = Number Int
                | Add Expression Expression
                | Subtract Expression Expression
                deriving (Show, Ord, Eq)

calculate :: Expression -> Int
calculate (Number x) = x
calculate (Add x y) = (calculate x) + (calculate y)
calculate (Subtract x y) = (calculate x) - (calculate y)
calculate (Add (Number  3) (Subtract (Number 2) (Number 1)))
