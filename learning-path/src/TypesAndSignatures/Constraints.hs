myFold :: (b -> a -> b) -> [a] -> b -> b
myFold _ [] initial = initial
myFold f (x : xs) initial = myFold f xs (f initial x)

-- mySum :: Num a => [a] -> a
mySum :: [Int] -> Int
mySum xs = myFold (+) xs 0

main :: IO ()
main = do
    print $ mySum [10, 20..100]