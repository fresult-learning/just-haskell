s0 :: String
s0 = "abc"

s1 :: [Char]
s1 = "\0088\x0058\o130"

main :: IO ()
main = do
    print s0
    print s1
