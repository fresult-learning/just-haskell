myMap :: (a -> b) -> [a] -> [b]
myMap _ [] = []
myMap f (x : xs) = f x : myMap f xs

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _ [] = []
myFilter f (x : xs) = if f x then x : myFilter f xs else myFilter f xs

myFold :: (b -> a -> b) -> [a] -> b -> b
myFold _ [] initial = initial
myFold f (x : xs) initial = myFold f xs (f initial x)

main :: IO ()
main = do
    print $ myMap show [10,20..100]
    print $ myFilter (\x -> mod x 2 == 0) [1..10]
    print $ myFold (+) [1..10] 100
