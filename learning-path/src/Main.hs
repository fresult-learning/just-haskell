module Main (main) where

-- main :: IO ()
-- main = do
  -- putStrLn "hello world"

main :: IO ()
main = do
  putStrLn $ greet "world" 
  print $ add 2 3
  print $ add' (4, 5)
  print $ "addOne: " ++ (show $  addOne 2)
  print $ "addOne': " ++ (show $ addOne' 2)
  print $ "addOne'': " ++ (show $ addOne'' 2)
  print $ "addOne''': " ++ (show $ addOne''' 2)

greet :: String -> String
greet str = "Hello, " ++ str

add :: Num a => a -> a -> a
add x y = x + y

add' :: Num a => (a, a) -> a
add' (x, y) = x + y

addOne :: Int -> Int
addOne x = add 1 x

addOne' :: Int -> Int
addOne' = add 1

addOne'' :: Int -> Int
addOne'' x = (+) x 1

addOne''' :: Int -> Int
addOne''' = (+) 1
