main :: IO ()
main =
    let x = 5 :: Int
        y = 6 :: Int
    in print(x + y)
