{-# OPTIONS_GHC -Wno-overlapping-patterns #-}
readInts :: String -> [Int]
readInts str = let ws = words str in map read ws

minMax :: Ord a => [a] -> Maybe (a, a)
minMax _ = Nothing
minMax (x : xs) = Just $ foldr
    (\y (min', max') -> (if y < min' then x else min', if y > max' then y else max'))
    (x, x)
    xs

main :: IO ()
main = do
    content <- readFile "numbers.txt"
    let values = readInts content
        count = length values
        total = sum values
        mean :: Double
        mean = fromIntegral total / fromIntegral count
        range = minMax values
    print count
    print mean
    print total
    print range
