addBrackets :: String -> String
addBrackets str = "[" ++ str ++ "]"

result :: [String]
result = map addBrackets ["one", "two", "three"]

main :: IO ()
main = print result
