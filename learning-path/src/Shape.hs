data Point = Point Double Double
data Radius = Radius Double
data Shape = Circle Radius
  | Rectangle Point Point

surfaceAreaOf :: Shape -> Double
surfaceAreaOf (Circle (Radius r)) = pi * r ^ 2
surfaceAreaOf (Rectangle (Point x1 y1) (Point x2 y2)) = (abs(x2 - x1)) * (abs(y2 - y1))
