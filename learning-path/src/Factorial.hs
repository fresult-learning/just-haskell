factorial :: Int -> Int
factorial x = if x < 2 then 1 else x * factorial ((-) x 1)

main :: IO ()
main = print $ factorial 10
