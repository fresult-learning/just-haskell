module Haskell03 where

-- Single Line Comment
{-
  Multiple Line Comment
-}

-- Arithmetic Operators
a :: Int
a = 1 + 1 -- Infix Operator

a' :: Int
a' = (+) 1 1 -- Prefix Operator

b :: Int
b = 2 - 3

b' :: Integer
b' = (-) 2 3

c :: Int
c = 3 * 5

c' :: Int
c' = (*) 3 5

d :: Double
d = 4 / 4

d' :: Double
d' = (/) 4 4

e :: Int
e = div 5 2 -- Prefix Operator

e' :: Int
e' = 5 `div` 2 -- Infix Operator

f :: Int
f = mod 3 2

f' :: Int
f' = 3 `mod` 2

g :: Int
g = 2 ^ 3

g' :: Int
g' = (^) 2 3

-- h :: Int
h :: Integer
h = 2 ^ 3 ^ 4

h' :: Integer
h' = (2 ^ 3) ^ 4

it :: Int
it = maxBound :: Int

-- Custom Functions
hypotenuse :: Double -> Double -> Double
hypotenuse x y = sqrt(x ^ 2 + y ^ 2)

-- Composition Functions
double :: Int -> Int
double x = x * 2

square :: Int -> Int
square x = x ^ 2

doubleSquare :: Int -> Int
doubleSquare = square . double -- doubleSquare x = (square . double) x

-- Currying Functions
pow :: Int -> Int -> Int
pow y x = x ^ y

powTwo :: Int -> Int
powTwo = pow 2

